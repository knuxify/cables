import logging
import hydrus_api
import hydrus_api.utils
import ring
from typing import Optional
from cabling_bot.bot.config import ConfigKeys

logger = logging.getLogger(__name__)


class ClientMisconfigured(hydrus_api.HydrusAPIException):
    pass


class HydrusService:
    def __init__(
        self,
        # config: ConfigKeys,
        endpoint: str,
        api_key: str
    ):
        # config.new(
        #     "hydrus", "enable",
        #     "(bool) Toggle Hydrus client API integration to supplement R-18 media previews and reporting.",
        #     False
        # )
        # config.new(
        #     "hydrus", "endpoint",
        #     "(str) Hydrus client API endpoint URI. Cannot be of HTTPS schema due to technical limitations.",
        #     "http://127.0.0.1:45869/"
        # )
        # config.new(
        #     "hydrus", "api_key",
        #     "(str) Hydrus client API key.",
        #     None
        # )

        self.client = None
        try:
            _permissions = {hydrus_api.Permission.SEARCH_FILES}

            self.client = hydrus_api.Client(
                access_key=api_key,
                api_url=endpoint
            )

            # manually check permissions
            # (the library doesn't differentiate, only returning a bool)
            response = self.client.verify_access_key()
            granted_perms = set(response["basic_permissions"])
            missing_perms = (_permissions - granted_perms)
            if missing_perms:
                logger.error(
                    "Hydrus client API key missing permissions: "
                    + self._permset_to_csv(missing_perms)
                )

            _endpoint_ver = self.client.get_api_version()['version']
            logger.info(
                "Initialized Hydrus client API module; "
                + f"library_rev={self.client.VERSION}, "
                + f"endpoint_rev={_endpoint_ver}, "
                + f"endpoint={endpoint}, "
            )

            superfluous_perms = (granted_perms - _permissions)
            if superfluous_perms:
                logger.warning(
                    "Hydrus client API key overly permissive: "
                    + self._permset_to_csv(superfluous_perms)
                )
        except (
            RuntimeError,
            hydrus_api.ConnectionError,
            hydrus_api.InsufficientAccess
        ) as e:
            logger.warning(f"Hydrus client API initialization error: {e}")
            self.client = False

    def _permset_to_csv(self, s: set) -> str:
        _list = [hydrus_api.Permission(p).name for p in s]
        _csv = ", ".join(_list)
        return _csv

    @ring.lru()
    def _get_file_metadata(self, file_hash: str) -> Optional[dict]:
        # TODO: retry w/ exponential backoff
        response = self.client.get_file_metadata(hashes=[file_hash])

        metadata = response[0]
        # if not metadata["file_id"]:
        #     # file not found
        #     return None

        return metadata

    @_get_file_metadata.ring.key
    def _get_ring_key(self, a: str) -> str:
        return a

    def get_media_attributes(self, file_hash: str) -> Optional[dict]:
        metadata = self._get_file_metadata(file_hash)
        if not metadata:
            return None

        mimetype: str = metadata["mime"]

        if mimetype.startswith(('image', 'video')):
            attributes = {
                "width": metadata["width"],
                "height": metadata["height"]
            }
        if mimetype.startswith('video'):
            attributes.update({
                "duration": metadata["duration"],
                "has_audio": metadata["has_audio"]
            })

        attributes.update({
            "mimetype": mimetype,
            "size": metadata["size"]
        })

        return attributes

    def get_tags(self, file_hash: str) -> Optional[list]:
        metadata = self._get_file_metadata(file_hash)
        # logger.debug(metadata)
        if not metadata:
            return None

        if not metadata.get("service_names_to_statuses_to_display_tags", False):
            return None
        tag_domains: dict
        tag_domains = metadata["service_names_to_statuses_to_display_tags"]
        if not tag_domains.get("public tag repository", False):
            return None

        """ statuses (str):
            0: current
            1: pending
            2: deleted
            3: petitioned
        """
        ptr_statuses: list = tag_domains["public tag repository"].keys()
        if "0" not in ptr_statuses:
            # no tags
            return list()

        ptr_tags: list = tag_domains["public tag repository"]["0"]
        return ptr_tags
