import logging
import psycopg_pool
from psycopg import OperationalError
import psycopg.rows

logger = logging.getLogger(__name__)


class DatabaseService:
    def __init__(self, bot):
        params, params_redacted = self._get_config(bot)

        self.client = None
        try:
            self.client = psycopg_pool.ConnectionPool(kwargs=params)
            self.client.wait()
            logger.info(
                f"Connected to database (Postgres) with configuration: {params_redacted}"
            )
        except OperationalError:
            logger.info(f"Postgres connection pool misconfigured: {params_redacted}")

    def _get_config(self, bot) -> dict:
        params = {
            "host": bot.get_config("db.host", "172.18.100.100"),
            "port": bot.get_config("db.port", 5432),
            "user": bot.get_config("db.user", "cables"),
            "password": bot.get_config("db.password", None),
            "dbname": bot.get_config("db.database", "cables"),
            "row_factory": psycopg.rows.namedtuple_row,
        }
        params_redacted = params | {"password": "[REDACTED]"}
        return (params, params_redacted)