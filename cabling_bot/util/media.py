import mimetypes
import logging
from typing import Iterable, Optional, Union
from discord import Attachment, Message
from cabling_bot.util.matching import extract_all_urls

logger = logging.getLogger(__name__)


def message_type(message: Message) -> list:
    types = []

    if isinstance(message.attachments, Iterable):
        for attachment in message.attachments:
            if attachment.content_type.startswith(
                ("image/", "video/")
            ):
                if "🖼" not in types:
                    types.append("🖼")
            else:
                if "📎" not in types:
                    types.append("📎")

    if isinstance(message.embeds, Iterable):
        for embed in message.embeds:
            if embed.image or embed.video:
                if "🖼" not in types:
                    types.append("🖼")
                break

    return types


def list_post_images(
    msg: Message,
    links: Optional[list[str]] = None
) -> Optional[list[str]]:
    media, candidates = [], []

    candidates += msg.attachments
    if not links:
        candidates += extract_all_urls(msg.content)
    else:
        candidates += links

    for candidate in candidates:
        maybe_media = get_direct_image_link(candidate)
        if not maybe_media:
            continue
        media += [maybe_media]

    return media


def get_direct_image_link(
    obj: Union[Attachment, str]
) -> Optional[str]:
    # post attachments
    if type(obj) is Attachment:
        mime = obj.content_type
        if not mime:
            return None
        if mime.startswith("image/"):
            return obj.url

    # image links
    mime = mimetypes.guess_type(obj)[0]
    if not mime:
        return None
    if mime.startswith("image/"):
        return obj
