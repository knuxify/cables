import asyncio
from typing import (Optional, Union)
from discord import (AllowedMentions, Embed, Reaction, Member, User)
from cabling_bot.bot.constants import (Colours, Emojis, Symbols)
from cabling_bot.bot import CablesContext as Context


async def quorum_vote(
    ctx: Context,
    topic: str,
    voters: Union[Member, User],
    timeout: Optional[float] = 300.0,
) -> bool:
    """Seeks quorum consensus from a given set of users by reaction voting.

    Default timeout is 5 minutes."""

    # TODO: add mention of admin role?
    vote_text = f"{Emojis.ballot_box} Vote required {topic}"
    vote_msg = await ctx.message.reply(
        vote_text,
        mention_author=False,
        allowed_mentions=AllowedMentions.none()
    )
    await vote_msg.add_reaction(Emojis.confirm)

    def targeting(r: Reaction, u: User):
        return (r.message.id == vote_msg.id and u in voters)

    approvals = list()
    while not approvals or set(approvals) != set(voters):
        try:
            reaction: Reaction
            reactor: Union[Member, User]

            reaction, reactor = await ctx.bot.wait_for(
                'reaction_add', timeout=timeout, check=targeting
            )

            if reactor == ctx.guild.me:
                continue
            elif (
                reaction.is_custom_emoji()
                or reaction.emoji != Emojis.confirm
            ):
                await reaction.remove(reactor)
                # react_emojis = [r.emoji for r in vote_msg.reactions]
                # if Emojis.no_gesture not in react_emojis:
                #     await vote_msg.add_reaction(Emojis.no_gesture)
                # ctx.bot.loop.create_task(
                #     vote_msg.remove_reaction(Emojis.no_gesture, ctx.guild.me)
                # )
            else:
                approvals += [reactor]
        except asyncio.TimeoutError:
            topic = topic.removeprefix("to ").capitalize()
            err = (f"**Sanction:** {topic}\n"
                   + f"**Votes:** {len(approvals)}/{len(voters)}\n")
            if approvals:
                c = 1
                for m in approvals:
                    err += f"{Symbols.mdash} `{m}`\n"
                    c += 1

            embed = Embed(color=Colours.dark_goldenrod,
                          title="Vote quorum unattained",
                          description=err)
            await ctx.reply(embeds=[embed])
            await vote_msg.delete(delay=5.0)
            return False

    return True
