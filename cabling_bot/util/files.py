import hashlib
import os
import requests
import random
import time
import uuid

from typing import Optional


def checksum_file(path, chunk_size=65536, algorithm='sha256') -> str:
    checksum = hashlib.new(algorithm)
    with open(path, 'rb') as f:
        for chunk in iter(lambda: f.read(chunk_size), b''):
            checksum.update(chunk)
    return checksum.hexdigest()


def download_file(
    path: str,
    remote: str,
    chunk_size: int = 4086
) -> Optional[str]:
    """Downloads a remote file to local storage
    - Supports large files by chunking writes
    - Retries up to five times with exponential backoff and jitter
    - Cumulative time spent withing between attempts may be up to ~24 mins
    - Backoff algorithm: `(i**2)+rand(0, min(1, i/2))`

    :param remote: remote URI to download
    :param chunk_size: target size in bytes for each chunk of the stream
    :return: save location on disk, if successful, otherwise None
    """

    guid = uuid.uuid5(uuid.NAMESPACE_URL, remote)
    local = f"{path}/{str(guid)}"

    retries, interval = 5, 1.0
    for attempt in range(1, retries + 2):
        try:
            if attempt > 1:
                jitter = random.uniform(0, min(1, interval / 2))
                interval = (interval ** 2) + jitter
                time.sleep(interval)
            with requests.get(remote, stream=True) as r:
                r.encoding = r.apparent_encoding
                r.raise_for_status()
                with open(local, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=chunk_size):
                        if chunk:
                            f.write(chunk)
                return guid
        except requests.exceptions.RequestException:
            delete_file(local)

    return None


def delete_file(filepath: str) -> Optional[bool]:
    if not os.path.isfile(filepath):
        return None
    else:
        os.remove(filepath)
        return True
