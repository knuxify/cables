from typing import Optional, Union
from discord import (
    Guild, TextChannel, VoiceChannel,
    ForumChannel, StageChannel, Thread,
    DMChannel, Forbidden, NotFound,
)
from ..bot.types import MessageableChannel
from ..bot import CablesContext as Context
from ..bot import errors
from .user import can_send_dm
from .permissions import is_channel_readable


async def is_channel_sendable(
    bot,
    channel: MessageableChannel,
    fallback: Optional[MessageableChannel] = None,
) -> Optional[Union[MessageableChannel, bool]]:
    """Tests for permission to send messages in a given channel.

    Channel object must be one of type TextChannel, VoiceChannel, Thread, or DMChannel.

    Returns the channel on success, `None` for unknown, and otherwise `False`.
    """
    if not channel:
        if not fallback:
            err = "Must provide at least one channel object that is not None."
            raise ValueError(err)
        else:
            channel = fallback

    try:
        if type(channel) in (TextChannel, VoiceChannel, Thread):
            assert channel.can_send()
        elif isinstance(channel, DMChannel):
            assert await can_send_dm(channel, bot.user)
        else:
            raise TypeError
    except AssertionError as a:
        return a
    except TypeError:
        ok_types = "TextChannel, VoiceChannel, Thread, or DMChannel"
        raise TypeError(f"Channel object must be one of type {ok_types}")
    else:
        return channel


async def check_configured_channel(
    ctx: Context,
    module: str,
    key: str,
) -> Optional[Union[TextChannel, VoiceChannel, Thread]]:
    config = f"{module}.{key}"
    guild = ctx.guild
    channel_id = ctx.get_guild_key(guild.id, module, key)

    _prefix, _suffix = f"User tried to use `{ctx.command}`, but ", str()
    if not channel_id:
        _suffix = f"`{config}` isn't set"
    else:
        try:
            channel = await ctx.bot.fetch_channel(channel_id)
        except NotFound:
            _suffix = f"`{config}` is an invalid destination"
        except Forbidden:
            _suffix = f"lacking view perms to `{config}`"
        else:
            if channel.guild != guild:
                _suffix = f"`{config}` isn't part of this guild"
            elif type(channel) not in (TextChannel, VoiceChannel, Thread):
                _suffix = f"`{config}` isn't a valid channel type"
            elif not channel.permissions_for(guild.me).create_instant_invite:
                _suffix = f"lacking invite perms to `{config}`"
    if _suffix:
        err = _prefix + _suffix
        await errors.report_guild_error(ctx.bot, ctx, None, guild, err)
        return None

    return channel


def get_text_channels(guild: Guild) -> list[Union[TextChannel, ForumChannel]]:
    return guild.text_channels + guild.forum_channels


def get_voice_channels(guild: Guild) -> list[Union[VoiceChannel, StageChannel]]:
    return guild.voice_channels + guild.stage_channels


async def get_threads(guild: Guild) -> tuple[list[Thread]]:
    channels = get_text_channels(guild)
    all_threads: list[Thread] = list()
    for c in channels:
        all_threads += c.threads
        try:
            async for t in c.archived_threads(limit=None, private=False):
                all_threads += [t]
            async for t in c.archived_threads(limit=None, private=True):
                all_threads += [t]
        except Forbidden:
            pass

    public_threads = [t for t in all_threads if not t.is_private()]
    active_threads = [t for t in public_threads if not t.archived]
    private_threads = [t for t in all_threads if t.is_private()]

    return (
        active_threads,
        public_threads,
        private_threads
    )


def filter_guild_channels(
    guild: Guild,
    targets: list[int]
) -> Optional[list[TextChannel]]:
    if not targets:
        return None
    return [
        c for c in guild.text_channels if (
            c.type.name == "text"
            and is_channel_readable(c.permissions_for(guild.me))
            and (c.category.id in targets
                 or c.id in targets)
        )
    ]
