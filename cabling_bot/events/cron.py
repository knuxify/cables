import logging
import yaml
import aiocron
import psycopg_pool

from textwrap import dedent
from discord.ext import commands
from discord import option
from ..util.transform import strtobool

from typing import (NamedTuple, Optional, Union)
from psycopg import DatabaseError
from discord import (SlashCommandGroup, Embed, Colour)
from ..cmds.verifications import MemberVerificationManager
from ..bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
    CablesAppContext as ApplicationContext,
    CablesAutocompleteContext as AutocompleteContext,
    checks, errors,
)

logger = logging.getLogger(__name__)


def _autocomplete(ctx: AutocompleteContext) -> Optional[list]:
    state: Optional[bool]
    match ctx.command.name:
        case "enable":
            state = True
        case "disable":
            state = False
        case "show":
            state = None

    prefix = None
    if ctx.value:
        prefix = ctx.value.lower().replace(" ", "_")

    tasks = list()
    name: str
    for name, attr in ctx.cog.jobs.items():
        if (
            # global tasks
            attr["guild_iterable"]
            # tasks not in the requested state
            or (state and attr["state"] is not state)
            # tasks meeting input prefix, when given
            or (prefix and name.startswith(prefix))
        ):
            tasks += [name.replace("_", " ")]
    return tasks


class CronManager(Cog):
    """Scheduled task management. See `help cron` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

        self.db: psycopg_pool.ConnectionPool = bot.db
        q = """\
            CREATE TABLE IF NOT EXISTS guild_cron_jobs(
                id              serial      PRIMARY KEY,
                guild           bigint      NOT NULL,
                "name"          text        NOT NULL,
                "enabled"       bool        NOT NULL DEFAULT 'false',
                config          json        NULL
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_guildcronjobs_guildname
                ON guild_cron_jobs (guild, "name");
            """
        with self.db.connection() as conn:
            try:
                conn.execute(q)
            except DatabaseError as e:
                logger.debug("Cog init error: " + e)

        self.jobs: dict[str, dict] = dict()
        for name in dir(__class__):
            if not name.endswith('_job'):
                continue

            func = getattr(self, name)
            args = yaml.safe_load(dedent(func.__doc__))
            name = name.removesuffix("_job")

            self.jobs[name] = dict(
                job=aiocron.crontab(
                    args["spec"],
                    func=func,
                ),
                spec=args["spec"],
                guild_iterable=args["guild_iterable"],
                description=args["description"],
                state=True,
            )

        logger.debug(f"Loaded scheduler jobs: {self.jobs.keys()}")

    @property
    def verification_mgr(self) -> MemberVerificationManager:
        return self._bot.get_cog("MemberVerificationManager")

    ##
    ## GLOBAL MANAGEMENT
    ##

    def _list(self, guild_iterable: bool = None) -> Optional[dict]:
        _jobs = dict()
        for name, attr in self.jobs.items():
            if ((
                guild_iterable is True
                and not attr["guild_iterable"]
            ) or (
                guild_iterable is False
                and attr["guild_iterable"]
            )):
                continue
            _jobs[name] = attr
        return _jobs

    def _exists(self, name: str) -> bool:
        return bool(self.jobs.get(name, None))

    def _fmt_details(
        self,
        name: str,
        details: Optional[dict] = None,
        overrides: Optional[dict] = dict()
    ) -> str:
        if not details:
            details = self.jobs[name]
        details |= overrides
        fields = [
            f"**Name:** {name}",
            f"**Description:** {details['description']}",
            f"**Recurrence:** `{details['spec']}`",
            f"**Enabled:** {details['state']}",
        ]
        return "\n".join(fields)

    def _start(self, name: str) -> bool:
        if not self._exists(name):
            return False

        self.jobs[name]["job"].start()
        self.jobs[name]["state"] = True
        return True

    def _stop(self, name: str) -> bool:
        if not self._exists(name):
            return False

        self.jobs[name]["job"].stop()
        self.jobs[name]["state"] = False
        return True

    @commands.group(name="cron")
    async def scheduler(self, ctx: Context):
        """Commands for managing internal scheduled tasks."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    @commands.is_owner()
    @scheduler.command(name="list", aliases=["show", "ls"])
    async def list_cmd(self, ctx: Context) -> None:
        """Shows a list of the bot's scheduled tasks"""

        tasks = self._list()
        if not tasks:
            await ctx.fail_msg("No scheduled tasks loaded.")
            return

        embed = Embed(
            colour=Colour.teal(),
            title="Scheduled Tasks",
            description="A list of the bot's internal scheduled tasks and their state.",
        )

        for name, details in tasks.items():
            text = self._fmt_details(name, details=details)
            embed.add_field(
                name="Task Detail",
                value=text,
                inline=True
            )

        await ctx.reply(embeds=[embed])

    @checks.is_guild_owner()
    @scheduler.command(name="get", aliases=["fetch"])
    async def get_cmd(self, ctx: Context, name: str) -> None:
        """Shows the schedule, state and configuration for a specific task."""

        if not self._exists(name):
            await ctx.fail_msg("Task doesn't exist.")
            return

        embed = Embed(
            title="Task Detail",
            description=self._fmt_details(name),
            colour=Colour.teal(),
        )

        await ctx.reply(embeds=[embed])

    @commands.is_owner()
    @scheduler.command(name="start")
    async def start_cmd(self, ctx: Context, name: str) -> None:
        """Activates scheduled execution of a specific task."""

        if not self._start(name):
            await ctx.fail_msg("Task doesn't exist.")
            return

        await ctx.react_success()

    @commands.is_owner()
    @scheduler.command(name="stop")
    async def stop_cmd(self, ctx: Context, name: str) -> None:
        """De-activates scheduled execution of a specific task."""

        if not self._stop(name):
            await ctx.fail_msg("Task doesn't exist.")
            return

        await ctx.react_success()

    @commands.is_owner()
    @scheduler.command(name="stopall", aliases=["sa", "shutdown"])
    async def stopall_cmd(self, ctx: Context) -> None:
        """De-actives scheduling for all tasks registered with the bot."""

        tasks = self.jobs.keys()
        if not tasks:
            await ctx.fail_msg("No scheduled tasks loaded.")
            return

        for task in tasks:
            self._stop(task)

        await ctx.react_success()

        text = "Tasks paused: "
        text += ", ".join(self.jobs.keys())
        await ctx.reply(text)

    ##
    ## GUILD MANAGEMENT
    ##

    def _db_get_guild_state(self, guild: int, name: str) -> bool:
        q = """
            SELECT "enabled" FROM guild_cron_jobs
            WHERE guild = %s AND "name" = %s
        """
        v = (guild, name)
        with self.db.connection() as conn:
            cursor = conn.execute(q, v)
            row: NamedTuple = cursor.fetchone()
            if ((
                type(row.enabled) is str
                and not strtobool(row.enabled)
            ) or (
                type(row.enabled) is bool
                and not row.enabled
            )):
                return False
            return True

    def _db_set_guild_state(self, guild: int, name: str, state: bool) -> bool:
        q = """
            UPDATE guild_cron_jobs
            SET "enabled" = %s
            WHERE
                guild = %s
                AND "name" = %s
        """
        v = (state, guild, name)
        with self.db.connection() as conn:
            try:
                cur = conn.execute(q, v)
            except DatabaseError:
                return False
            else:
                if not cur.rowcount:
                    return False
                return True

    async def toggle_guild_state(
        self, guild: int, task: str, state: bool,
        ctx: Optional[ApplicationContext] = None,
    ) -> Union[str, bool]:
        pretty_state = "Enabled" if state else "Disabled"
        pretty_task = task.replace("_", " ")
        if not self._db_set_guild_state(guild, task, state):
            await errors.report_database_error(
                self._bot, ctx,
                f"{pretty_state} {pretty_task}", None
            )
            return False
        return f"✅ {pretty_state} {pretty_task} task"

    cmd_group = SlashCommandGroup(
        "scheduler",
        "Management commands for the task scheduler.",
        guild_only=True,
        checks=[checks.is_guild_owner().predicate]
    )

    @cmd_group.command(name="show")
    @option("task", description="Name of the scheduled task", autocomplete=_autocomplete)
    async def get_slashcmd(self, ctx: ApplicationContext, task: str) -> None:
        """Shows the schedule, state and configuration for a specific task."""

        name = task.replace(" ", "_")
        if not self._exists(name):
            await ctx.respond("❌ Task doesn't exist.", ephemeral=True)
            return

        state = self._db_get_guild_state(ctx.guild.id, name)
        await ctx.respond(embed=Embed(
            title="Task Detail",
            description=self._fmt_details(
                name, overrides=dict(state=state)
            ),
            colour=Colour.teal()
        ))

    @cmd_group.command(name="enable")
    @option("task", description="Name of the scheduled task", autocomplete=_autocomplete)
    async def enable_slashcmd(self, ctx: ApplicationContext, task: str) -> None:
        """Enable automatic execution of a scheduled task."""
        task = task.replace(" ", "_")
        if (text := await self.toggle_guild_state(
            ctx.guild_id, task, True, ctx=ctx
        )):
            await ctx.respond(text)

    @cmd_group.command(name="disable")
    @option("task", description="Name of the scheduled task", autocomplete=_autocomplete)
    async def disable_slashcmd(self, ctx: ApplicationContext, task: str) -> None:
        """Disable automatic execution of a scheduled task."""
        task = task.replace(" ", "_")
        if (text := await self.toggle_guild_state(
            ctx.guild_id, task, False, ctx=ctx
        )):
            await ctx.respond(text)

    #############################
    # SCHEDULED TASK DEFINITONS #
    #############################

    # async def test_job(self):
    #     """
    #     spec: '*/1 * * * *'
    #     guild_iterable: false,
    #     description: 'A simple demonstration of a functional cronjob.'
    #     """
    #
    #     await self._bot.wait_until_ready()
    #
    #     gid = self._bot.get_config("privileged.dev_guild")
    #     guild = discord.utils.find(lambda g: g.id == gid, self._bot.guilds)
    #     try:
    #         if guild.system_channel.can_send():
    #             text = "Scheduled task 'test' is executing."
    #             await guild.system_channel.send(text)
    #     except KeyError:
    #         pass

    async def member_autopurge_job(self):
        """
        spec: '12 */1 * * *'
        guild_iterable: true
        description: 'Purge unverified members across enabled guilds.'
        """
        self._bot.dispatch("member_purge")

    async def message_aggregation_job(self):
        """
        spec: '37 0 */1 * *'
        guild_iterable: true
        description: 'Update message statistics every day across enabled guilds.'
        """
        self._bot.dispatch("aggregate_messages")


def setup(bot: Bot):
    bot.add_cog(CronManager(bot))
