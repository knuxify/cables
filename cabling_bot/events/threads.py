import logging
from discord.ext import commands
from discord import Thread
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
)

logger = logging.getLogger(__name__)


class ThreadManager(Cog):
    """Manages the bot's presence in threads as it receives events about them."""

    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot

    @commands.Cog.listener("on_thread_create")
    async def handle_create(self, thread: Thread) -> None:
        """Joins a thread as it's created."""
        await thread.join()

    # NOTE: don't implement this until we have a solution for tracking
    # if the client has been removed from a thread before
    # @commands.Cog.listener("on_thread_unarchived")
    # async def handle_unarchival(self, before: Thread, after: Thread) -> None:
    #     """Joins a thread as it's unarchived by a member."""
    #     await after.join()

    @commands.Cog.listener("on_thread_archived")
    async def handle_archive(self, thread: Thread) -> None:
        """Unarchives a persistent thread as it's archived."""
        persistent_threads = self._bot.get_guild_key(
            thread.guild.id, "utility", "persistent_threads"
        )
        if (
            persistent_threads
            and thread.id in persistent_threads
        ):
            await thread.unarchive()


def setup(bot: Bot):
    bot.add_cog(ThreadManager(bot))
