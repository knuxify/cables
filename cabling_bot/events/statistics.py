import asyncio
import logging
from typing import Optional
import psycopg_pool

from discord.ext import commands
from ..events.cron import CronManager
from ..util import statistics
from ..util.messages import get_message
from ..util.channels import filter_guild_channels
from ..bot.errors import report_guild_error

from discord import (Guild, Message, TextChannel)
from ..bot import (
    CablesCog as Cog,
    CablesBot as Bot,
)

logger = logging.getLogger(__name__)


class MsgAggregationEventMgr(Cog):
    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot
        self.db: psycopg_pool.ConnectionPool = bot.db

    @property
    def scheduler(self) -> CronManager:
        return self._bot.get_cog("CronManager")

    async def _get_guild_config(
        self, gid: int,
    ) -> Optional[tuple[list[TextChannel], Message, int]]:
        guild = self._bot.get_guild(gid)
        chid = self._bot.get_guild_key(gid, "statistics", "summary_channel")
        if not chid:
            await self.scheduler.toggle_guild_state(
                gid, "message_aggregation", False
            )
            text = ("Statistics generation was scheduled to run, but it's not "
                    + "configured. Task has been disabled. Run `/stats setup`")
            await report_guild_error(self._bot, None, None, guild, text)
            return None

        mid = self._bot.get_guild_key(gid, "statistics", "summary_msg")
        if not (msg := await get_message(self._bot, guild, chid, mid)):
            config = self.init_guild_config(self._bot.db, guild.id)
            channel = guild.get_channel(chid)
            msg = await statistics.configure_guild(self._bot, config, channel)

        chids = [c.id for c in statistics.get_targets(self._bot, guild)]
        if not (channels := filter_guild_channels(guild, chids)):
            text = "No text channels found for message aggregation"
            await report_guild_error(self._bot, None, None, guild, text)
            return None

        settings = (channels, msg)
        return settings

    @commands.Cog.listener("on_aggregate_messages")
    async def cron_job(self) -> None:
        q = """
            SELECT guild, config
            FROM guild_cron_jobs
            WHERE
                "name" = 'message_aggregation'
                AND "enabled" = 'true';
        """
        with self.db.connection() as conn:
            cursor = conn.execute(q)
            if not cursor.rowcount:
                return
            rows = cursor.fetchall()

        for target, config in rows:
            tup = await self._get_guild_config(target)
            if not tup:
                continue
            (targets, msg) = tup
            guild = targets[1].guild

            j = {"job": "message_aggregation",
                 "guild": guild.id,
                 "days": config["days"]}
            log = {"CRONJOB_START": j}
            logger.info(log)

            await statistics.job(targets, msg, days=config["days"])

            log = {"CRONJOB_FINISH": j}
            logger.info(log)

            await asyncio.sleep(60)


def setup(bot: Bot):
    bot.add_cog(MsgAggregationEventMgr(bot))
