import re
import hashlib
import logging
from discord.ext import commands
from discord import (ApplicationContext, Guild, Message, option)
from cabling_bot.bot import (CablesCog as Cog, CablesBot as Bot, checks)
from cabling_bot.bot.constants import Emojis

logger = logging.getLogger(__name__)

#
# Implementation based on Amulets v1.1
# https://text.bargains/amulet/
#
#   Rarities deviate from spec, taking inspiration
#   from various MMORPG item qualities to an extent
#
AMULET_QUALITY = dict(
    enumerate([
        False, False, False, False,
        "Fine",
        "Resplendent",
        "Exotic",
        "Exalted",
        "Legendary",
        "Mythic",
        "Beyond mythic",
        "Harmonic",
        "Transcendent",
        "Quantum"
        "Imaginary",
        "Omnipotent",
    ])
)
AMULET_MATCH_REGEX = r"8{4,}"
AMULET_EXCLUDE_REGEX = bytes(r"([\.\,;\!\?=\*\+`~-]{4,}|['\"\|@#\$%\^\&\+\/\]]{2,})+", "utf-8")


class AmuletManager(Cog):
    """Amulets, a form of GETs with 'modern' style. See `help craft` for more info."""
    def __init__(self, bot: Bot):
        self._bot = bot
        bot.config_keys.new(
            "feature_flags",
            "amulets",
            "(bool) Toggles Amulet (#242) detection and notification guild-wide.",
            False
        )

    def enabled(self, guild: Guild) -> bool:
        return self._bot.get_guild_key(
            guild.id,
            "feature_flags", "amulets"
        )

    def identify_amulet(self, input: str):
        candidate = input.encode("utf-8")
        if (
            len(input) < 5
            or len(input) > 64
            or re.findall(AMULET_EXCLUDE_REGEX, candidate)
        ):
            return None

        hash = hashlib.sha256(candidate).hexdigest()
        matches = re.findall(AMULET_MATCH_REGEX, hash)
        if not matches:
            return False

        best = max(matches)
        length = len(best)
        quality = AMULET_QUALITY.get(length, "Incomprehensible")
        if not quality:
            return False

        text = f"{Emojis.amulet} {quality} amulet"
        text += f" ({hash.replace(best, f'**{best}**')})"
        return text

    @commands.Cog.listener("on_message_amulet_detected")
    async def send_amulet_reply(self, msg: Message, text: str) -> None:
        if not self.enabled(msg.guild):
            return
        await msg.reply(text, mention_author=False)

    @checks.is_feature_enabled("amulets")
    @commands.slash_command(name="craft")
    @option("poem", str, description="Your beautiful poem (must be between 6 and 64 characters)")
    async def craft_amulet(self, ctx: ApplicationContext, poem: str) -> None:
        """Submit a poem for analysis."""

        quote_lines = [f"> {line}\n" for line in poem.split("\n")]
        quoted_poem = "\n".join(quote_lines)

        if not poem:
            return await ctx.respond(
                quoted_poem + "You didn't sing to me, love~",
                ephemeral=True
            )

        amulet = self.identify_amulet(poem)

        if amulet is None:
            return await ctx.respond(
                quoted_poem + "Not poetic enough.",
                ephemeral=True
            )
        elif amulet is False:
            return await ctx.respond(quoted_poem + "Not an amulet.")

        await ctx.respond(quoted_poem + amulet)


def setup(bot: Bot):
    bot.add_cog(AmuletManager(bot))
