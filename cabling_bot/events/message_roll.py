import re
import logging

from discord.ext import commands
from discord.utils import snowflake_time
from ..util import matching

from typing import Optional, Union
from discord import (
    Interaction, Message,
    TextChannel, Thread, VoiceChannel
)
from ..bot.types import MessageableChannel
from ..bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesAppContext as ApplicationContext
)


logger = logging.getLogger(__name__)

# https://s4s.fandom.com/wiki/List_of_Checks
GET_REGEX = r"(\d)\1+$"
GET_PREFICES = dict(
    enumerate([
        None, None,
        "nice", "nice",
        "sweet",
        "amazing",
        "excellent",
        "euphric",
        "zOMG", "zOMG", "zOMG"
    ])
)
GET_NAMES = dict(
    enumerate([
        False, False,
        "dubs",
        "trips",
        "quads",
        "quints",
        "sexes",
        "septs",
        "octs",
        "nons",
        "decs",
    ])
)

ROLL_MODULE_KEYS = [
    "timestamp_matches",
    "dubs",
    "trips",
    "quads"
]


class RollManager(Cog):
    """RNG event and command module. See `help <roll>` for more info."""
    def __init__(self, bot: Bot):
        self._bot = bot

        bot.config_keys.new(
            "feature_flags", "rolls",
            "(bool) Toggles the RNG-based rolls feature on or off for the guild.",
            False
        )
        bot.config_keys.new(
            "rolls", "channels",
            "(list[int]) Limit automatic GET detection to a subset of channels. Default is to be enabled guild-wide.",
            None
        )

        bot.config_keys.new(
            "rolls", "timestamp_matches",
            "(bool) Toggles matching against UNIX timestamps in both automatic and command-triggered rolls. If disabled, only message IDs are compared.",
            True
        )

        # TODO: replace with a single setting for the maximum number of repeating digits to allow for
        bot.config_keys.new(
            "rolls", "dubs",
            "(bool) Toggles doubles in automatic message rolls.",
            False
        )
        bot.config_keys.new(
            "rolls", "trips",
            "(bool) Toggles triples in automatic message rolls.",
            False
        )
        bot.config_keys.new(
            "rolls", "quads",
            "(bool) Toggles quads in automatic message rolls.",
            False
        )

    def enabled(
        self,
        channel: Optional[MessageableChannel] = None,
        feature: Optional[str] = None
    ) -> bool | list[MessageableChannel]:
        if (
            (channel and not feature
             and type(channel) not in (TextChannel, Thread, VoiceChannel))
            or not channel
        ):
            raise ValueError("Must pass channel argument.")

        gid = channel.guild.id
        toggle = self.get_guild_key(gid, "feature_flags", "rolls")
        destinations = self.get_guild_key(gid, "rolls", "channels")
        is_target = (toggle and destinations
                     and matching.search_channels(channel, destinations))
        if not feature:
            if not (toggle and is_target):
                # Feature is disabled or target isn't configured
                return False
            elif type(toggle) is bool and not destinations:
                # Feature enabled guild-wide
                return toggle
            else:
                return True
        elif feature in ROLL_MODULE_KEYS:
            v = self.get_guild_key(gid, "rolls", feature)
            if self._bot.environment == "development":
                debug = ("GET configuration retrieved; "
                         + f"guild={gid}, key={feature}; value={v}>")
                logger.debug(debug)
            return v
        else:
            return None

    def _is_get(self, candidate: str) -> tuple[str, int]:
        match = re.search(GET_REGEX, candidate)
        if not match:
            return (False, 0)

        match = match[0]
        length = len(match)
        get = GET_NAMES.get(length, None)
        if get is False:
            return (False, 0)
        elif get is None:
            return (f"**MYTHIC GET!! ({candidate})**", length)
        else:
            text = str()

        prefix = GET_PREFICES.get(length, None)
        if prefix:
            text += prefix + " "

        numeral = int(match[0])
        if numeral == 0:
            text += "clear" + " "
        elif numeral == length:
            text += "pure" + " "

        text += get
        if length > 2:
            text += "!" * (length - 2)
        text += f" (FMT {candidate.replace(match, f'**{match}**')})"

        return (text, length)

    def parse_message(
        self, source: Union[Message, Interaction]
    ) -> Optional[tuple[str, str]]:
        msg_get, msg_length = self._is_get(str(source.id))
        timestamp = snowflake_time(source.id).strftime("%s")
        ts_get, ts_length = self._is_get(timestamp)

        if not (ts_get or msg_get):
            return False
        elif ts_length > msg_length:
            text = ts_get.replace("FMT", "🕝")
            fmt = "timestamp"
        else:
            text = msg_get.replace("FMT", "❄")
            fmt = "snowflake"
        return (fmt, text)

    @commands.slash_command(name="roll")
    async def roll(self, ctx: ApplicationContext) -> None:
        """Roll for your chances at a GET!!1"""
        get = self.parse_message(ctx.interaction)
        if not get:
            reply = f"Sorry about your luck, fam! ({ctx.interaction.id})"
            return await ctx.respond(reply, delete_after=5)
        fmt, match = get
        self._bot.dispatch(
            "message_get_detected",
            ctx, fmt, match
        )

    def _log(
        self, source: Union[ApplicationContext, Message],
        fmt: str, text: str
    ) -> None:
        if self._bot.environment == "development":
            log = "GET!! "
            log += (f"<guild={source.guild.id}, "
                    + f"channel={source.channel.id}, "
                    + f"message={source.id}, "
                    + f"timestamp={source.created_at}, "
                    + f"format={fmt}, "
                    + f"text=\"{text}>\"")
            logger.debug(log)

    def _can_reply(self, source: Union[ApplicationContext, Message]) -> bool:
        if (
            source.content != "🎲"
            and not self.enabled(channel=source.channel)
        ):
            if self._bot.environment == "development":
                debug = ("MESSAGE_GET_DETECTED returning None "
                         + "returning None on message-type match "
                         + "due to roll feature being disabled")
                logger.debug(debug)
            return False
        return True

    @commands.Cog.listener("on_message_get_detected")
    async def send_get_reply(
        self, source: Union[ApplicationContext, Message],
        fmt: str, text: str
    ) -> None:
        self._log(source, fmt, text)
        c = source.channel
        debug = "MESSAGE_GET_DETECTED returning None "
        if not self._can_reply(source):
            return None

        if (
            fmt == "timestamp"
            and not self.enabled(channel=c, feature="timestamp_matches")
        ):
            if type(source) is Message:
                if self._bot.environment == "development":
                    debug += ("on message-type match "
                              + "due to timestamp matches being disabled")
                    logger.debug(debug)
                return None
            elif type(source) is ApplicationContext:
                reply = f"Sorry about your luck, fam! ({source.interaction.id})"
                return await source.respond(reply, delete_after=5)

        if type(source) is Message:
            if (source.content != "🎲"
                and ((not self.enabled(channel=c, feature="dubs")
                      and "dubs" in text)
                     or (not self.enabled(channel=c, feature="trips")
                         and "trips" in text)
                     or (not self.enabled(channel=c, feature="quads")
                         and "quads" in text))):
                if self._bot.environment == "development":
                    debug += ("returning None on message-type match "
                              + "due to disabled match type")
                    logger.debug(debug)
                return None
            await source.reply(text, mention_author=False)
        elif type(source) is ApplicationContext:
            await source.respond(text)


def setup(bot: Bot):
    bot.add_cog(RollManager(bot))
