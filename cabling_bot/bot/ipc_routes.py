from discord.ext import commands, ipc
from discord.utils import utcnow
from discord import TextChannel
from datetime import timedelta
import logging
import json

from cabling_bot.bot.config import GuildConfig
from cabling_bot.util.statistics import _scan_channels
from cabling_bot.util import transform

logger = logging.getLogger(__name__)


class IpcRoutes(commands.Cog):
    purposes = {
        "colors.role_position": "role_id",
        "colors.allowed_role": "role_id",
        "content_scanning.channels": "channel_id/category_id",
        "invites.join_channel": "channel_id",
        "invites.user_limit": "generic",
        "namethrall.namedom_role": "role_id",
        "namethrall.namesub_role": "role_id",
        "member_verification.application_time_limit": "generic",
        "member_verification.role": "role_id",
        "moderation.report_channel": "channel_id",
        "notifications.account_age_threshold": "generic",
        "notifications.public_announce_channel": "channel_id",
        "notifications.thread_watch_channels": "channel_id",
        "notifications.tombstone_exemptions": "channel_id/category_id/user_id",
        "notifications.vc_activity": "channel_id",
        "preview.channels": "channel_id/category_id",
        "privileged.admin_proxy_role": "role_id",
        "privileged.admin_role": "role_id",
        "privileged.channel_moderator_role": "role_id",
        "privileged.global_moderator_role": "role_id",
        "privileged.mod_channel": "channel_id",
        "privileged.owner_proxy_role": "role_id",
        "privileged.staff_role": "role_id",
        "privileged.system_channel": "channel_id",
        "rolls.channels": "channel_id",
        "statistics.summary_channel": "channel_id",
        "statistics.summary_targets": "channel_id/category_id",
        "user_watch.interval": "generic",
        "user_watch.report_channel": "channel_id",
        "utility.persistent_threads": "thread_id",
    }

    required = []

    def __init__(self, bot):
        self.bot = bot
        self.get_config_schema()

    @ipc.server.route()
    async def get_bot_version(self, data=None) -> dict:
        """Gets the bot's current version."""
        return {"version": self.bot.version}

    @ipc.server.route()
    async def get_joined_guild_ids(self, data=None) -> dict:
        """Gets a list of IDs of guilds the bot is in."""
        return {"ids": [guild.id for guild in self.bot.guilds]}

    @ipc.server.route()
    async def get_config_schema(self, data=None) -> dict:
        """Gets the config key schema, to be parsed by the web UI."""
        config_keys = self.bot.config_keys.get(None, None)

        _config_keys = {}
        for key in config_keys:
            full_name = f"{key.module}.{key.key}"
            # DEVELOPER'S NOTE: These would be nice to have as keys in the
            # configuration schema:

            # Get title
            title = key.key.replace("_", " ").capitalize()

            # Get type from description
            _description = key.description.split(" ", 1)
            type = _description[0][1:-1]
            description = _description[1]

            # Get purpose
            purpose = "generic"
            if type in ("int", "list[int]") and full_name in self.purposes:
                purpose = self.purposes[full_name]
            elif full_name in self.purposes:
                logger.warn(f"Tried to return purpose for key but type is {type}")

            _config_keys[full_name] = {
                "id": key.id,
                "module": key.module,
                "key": key.key,
                "title": title,
                "type": type,
                "description": description,
                "default": key.default,
                "purpose": purpose,
                "required": full_name in self.required,
            }

        return _config_keys

    @ipc.server.route()
    async def get_config_values(self, data) -> dict:
        """
        Takes a guild ID and a list of module.key names and gets
        the key contents.
        """
        output = {}
        for _key in data.keys:
            module, key = _key.split(".")
            output[_key] = self.bot.config_data[data.guild_id][module][key]

            if isinstance(output[_key], set):
                output[_key] = list(output[_key])
        return output

    @ipc.server.route()
    async def set_config_values(self, data):
        """Takes a dictionary with module.key: value pairs and sets the keys."""
        guild_config = GuildConfig(self.bot.db, data.guild_id)

        values = json.loads(data.data)

        for _key, value in values.items():
            module, key = _key.split(".")
            result = guild_config.set(module, key, value)
            logger.info(f"{module}, {key}, {value}")

            if result:
                # Update the config cache
                if type(value) is str:
                    value = transform.strtoliteral(value)
                if isinstance(value, set):
                    value = list(value)
                self.bot.config_data[data.guild_id][module][key] = value

    @ipc.server.route()
    async def get_guild_data(self, data) -> dict:
        """Gets information about the guild with the given ID."""
        guild = None
        for found_guild in self.bot.guilds:
            if found_guild.id == data.guild_id:
                guild = found_guild
                break

        if not guild:
            return {"error": "Bot not in guild", "code": 500}

        _guild = {}
        _guild["id"] = guild.id
        _guild["name"] = guild.name
        _guild["icon_url"] = guild.icon.url
        _guild["member_count"] = guild.member_count

        _guild["categories"] = []
        category_ids = []
        for category in guild.categories:
            _guild["categories"].append(
                {
                    "id": category.id,
                    "name": category.name,
                    "channels": [channel.id for channel in category.channels],
                }
            )
            category_ids.append(category.id)

        _guild["channels"] = []
        for channel in guild.channels:
            _guild["channels"].append(
                {
                    "id": channel.id,
                    "name": channel.name,
                    "type": channel.type,
                    "category": channel.category
                    and _guild["categories"][category_ids.index(channel.category.id)],
                }
            )

        _guild["roles"] = []
        for role in guild.roles:
            _guild["roles"].append(
                {
                    "id": role.id,
                    "name": role.name,
                    "position": role.position,  # todo: we're not using this for hierarchy data but this may still not be correct according to pycord docs
                }
            )

        _guild["members"] = []
        for member in guild.members:
            _guild["members"].append(
                {
                    "id": member.id,
                    "name": member.name,
                    "discriminator": member.discriminator,
                    "nick": member.nick,
                    "roles": [role.id for role in member.roles],
                    "avatar": member.display_avatar.url,
                }
            )

        return _guild

    @ipc.server.route()
    async def get_guild_stats(self, data) -> dict:
        """Gets the guild's statistics."""
        output = {}
        days = data.days

        guild = None
        for found_guild in self.bot.guilds:
            if found_guild.id == data.guild_id:
                guild = found_guild
                break

        if not guild:
            return {"error": "Bot not in guild", "code": 500}

        text_channels = [
            channel
            for channel in guild.channels
            if isinstance(channel, TextChannel) and channel.category
        ]

        now = utcnow()
        after = now - timedelta(days + 1)

        (active_chans, parts, msgs, media, links) = await _scan_channels(
            text_channels, after, now
        )

        output["active_chans"] = active_chans
        output["parts"] = parts
        output["msgs"] = msgs
        output["media"] = media
        output["links"] = links

        return output


def setup(bot):
    bot.add_cog(IpcRoutes(bot))
