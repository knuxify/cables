import logging
import inspect
from typing import Callable, Coroutine, Union
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
)
from cabling_bot.util.migrations import MigrationManager
from cabling_bot.util.guild_invites import GuildInvitesController
from discord.ext import commands

logger = logging.getLogger(__name__)


class DbMigrations(Cog):
    """Database migration functions for bot upgrades. See `help migrate` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self.migration_mgr = MigrationManager(bot)
        self.invite_mgr = GuildInvitesController(bot)

    async def _migrate(
        self,
        method: Union[Coroutine, Callable],
        args: list
    ) -> tuple:
        if inspect.isawaitable(method):
            return await (method)(*args)
        else:
            return (method)(*args)

    @commands.is_owner()
    @commands.command("migrate")
    async def migrate(self, ctx: Context) -> None:
        """Run the latest database migration code.

        May only be used by the bot operators."""

        if ctx.bot.version not in self.migration_mgr.revs:
            await ctx.fail_msg("No migration to run!")
            return

        _rev = "v" + ctx.bot.version.replace(".", "_")
        _method = getattr(self.migration_mgr, _rev)
        _method_args = []

        failures, successes = [], 0
        for guild in ctx.bot.guilds:
            _guild_args = [guild]
            if _rev == "v0_8_32":
                for name in ["member_autopurge", "message_aggregation"]:
                    _method_args = [*_guild_args, name, False]
                    if name == "message_aggregation":
                        _method_args += [dict(days=7)]
                    else:
                        _method_args += [None]
                    _f, _s = await self._migrate(_method, _method_args)
                    if _s is None:
                        continue
                    failures += _f
                    successes += _s
            elif _rev == "v0_8_24":
                response = await guild.invites()
                remote = self.invite_mgr._transform_to_dict(response)
                known = await self.invite_mgr._list_known_invites(
                    guild, only_active=False
                )

                _guild_args += [known]
                for code, invite in remote.items():
                    _method_args = [*_guild_args, code, invite]
                    _f, _s = await self._migrate(_method, _method_args)
                    failures += _f
                    successes += _s
            else:
                _method_args += [_guild_args]
                _f, _s = await self._migrate(_method, _method_args)
                failures += _f
                successes += _s

        if failures:
            _failures = "\n".join(failures)
            logger.error(f"Query failures:\n{_failures}")

        if not successes:
            text = "Nothing to do!"
        else:
            total = (successes + len(failures))
            text = f"✅ Done! Query success rate: {successes}/{total}"
        await ctx.reply(text)


def setup(bot: Bot):
    bot.add_cog(DbMigrations(bot))
