from __future__ import annotations
from typing import TYPE_CHECKING

import logging
from discord.ext import commands

from ..util.discord import get_system_channel
from ..util.user import can_send_dm
from ..bot.errors import log_cmd_error
from ..events import ready

from typing import (NamedTuple, Union)
from discord import (AllowedMentions, Embed, HTTPException, Member, User)
from discord.errors import (Forbidden, NotFound)
from ..bot import (CablesCog as Cog, CablesContext as Context)

if TYPE_CHECKING:
    from typing import Optional
    from ..bot.types import MessageableChannel
    from cabling_bot.bot import CablesBot as Bot


logger = logging.getLogger(__name__)


class Destination(NamedTuple):
    guild_id: int
    target: Union[MessageableChannel, User, Member]


class OperatorManager(Cog):
    """Operator only maintenance commanding. See `help oper` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

        bot.config_keys.new(
            "general", "service_announcements",
            "(bool) Toggles receipt of service-wide bot-related announcements.",
            True
        )

    @property
    def websocket(self) -> ready.WebsocketManager:
        return self._bot.get_cog("WebsocketManager")

    @commands.is_owner()
    @commands.group()
    async def oper(self, ctx: Context):
        """Opeerator only maintenance commanding."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")
            return
        if not (self._bot.is_ready() or self.websocket._was_ready_before):
            await ctx.fail_msg("Bot not fully initialized yet!")
            return

    def _log_delivery_failure(
        self, ctx: Context,
        t: Destination, e: Exception
    ):
        code = "Delivery failed"
        origin = dict(issuer=ctx.author.id)
        details = dict(guild=t.guild_id,
                       channel_id=t.target.id,
                       channel_type=t.target.type.name)
        log_cmd_error(ctx, code, origin, e, details=details)

    def _format(self, ctx: Context, message: str) -> str:
        message = message.replace("@everyone", "")
        message = message.replace("@here", "")
        message = "> " + "\n> ".join(message.split("\n"))

        issuer = ctx.author.mention
        cmd = (f"{self._bot.command_prefix}config guild set general "
               + "service_announcements off")
        optout = ("**OPT-OUT NOTICE:** To stop receiving these updates, the "
                  + "server owner must issue the following command _on_ "
                  + f"their server: `{cmd}`")

        message = ("📌 **THIS IS A SERVICE-WIDE ANNOUNCEMENT SENT TO ALL "
                   + f"SERVERS/OWNERS!**\n\n{issuer} wrote:\n"
                   + message + "\n\n" + optout)
        return message

    async def _collect(self, ctx: Context) -> Optional[list[Destination]]:
        if self._bot.environment == "development":
            if not self._bot.operator:
                return None
            if not await can_send_dm(self._bot.operator, self._bot.user):
                return None
            return [Destination(guild_id=None, target=self._bot.operator)]

        destinations: list[Destination] = []
        for guild in self._bot.guilds:
            target = get_system_channel(self._bot, guild)
            if not target or (target and not target.can_send()):
                checks = (AssertionError, ValueError, TypeError, HTTPException)
                try:
                    assert guild.owner
                    assert await can_send_dm(guild.owner, self._bot.user)
                    target = guild.owner
                except checks as e:
                    log_cmd_error(ctx, "Target acquisition failed",
                                  dict(issuer=ctx.author.id), e)
            if not target:
                config = self.init_guild_config(self._bot.db, guild.id)
                config.set("general", "service_announcements", False)
                continue
            destinations += [Destination(guild_id=guild.id, target=target)]

        return destinations

    @oper.command(aliases=[])
    async def announce(
        self, ctx: Context,
        *, message: str,
    ) -> None:
        """Send an announcement to all integrated guilds' system channel or their owner.

        Mentioned will not ping, but regardless, `@everyone` and `@here` are stripped."""

        length = len(message)
        if length < 128:
            await ctx.fail_msg("Announcement too short (< 128 characters)")
            return

        embed = None
        message = self._format(ctx, message)
        if length > 2000 and length <= 4096:
            # TODO: format with author, etc.
            embed = Embed(description=message)
        elif length > 4096:
            await ctx.fail_msg("Announcement too long (> 4096 characters)")
            return

        targets = await self._collect(ctx)
        if not targets:
            await ctx.fail_msg("Nothing to do!", delete_after=-1)
            return

        for t in targets:
            if t.guild_id:
                key = ("general", "service_announcements")
                if not self.get_guild_key(t.guild_id, *key):
                    continue
            try:
                await t.target.send(
                    None if embed else message,
                    embeds=[embed] if embed else None,
                    allowed_mentions=AllowedMentions.none()
                )
            except (Forbidden, NotFound) as e:
                self._log_delivery_failure(ctx, t, e)

        await ctx.react_success()


def setup(bot: Bot):
    bot.add_cog(OperatorManager(bot))
