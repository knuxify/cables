import logging
from typing import Optional, Union

import discord.utils
from discord import Guild, Member, Role, User
from discord.errors import Forbidden
from discord.ext import commands
from discord import option

from cabling_bot.bot import (
    CablesCog as Cog,
    CablesAppContext as ApplicationContext,
    CablesBot as Bot
)
from cabling_bot.bot.errors import report_guild_error

logger = logging.getLogger(__name__)


class NamethrallManager(Cog):
    """Name game commands. See `help <namethrall/nick>` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        bot.config_keys.new(
            "namethrall",
            "namesub_role",
            "(int) Role that will allow others to change their nick",
            None
        )
        bot.config_keys.new(
            "namethrall",
            "namedom_role",
            "(int) Role that will forbid others from changing their nick",
            None
        )

    def get_roles(self, guild: Guild) -> Optional[tuple[Role]]:
        gid, roles = guild.id, list()
        for k in ("namesub", "namedom"):
            if not (role := discord.utils.find(
                lambda r:
                r.id == self.get_guild_key(gid, "namethrall", f"{k}_role"),
                guild.roles
            )):
                role = discord.utils.find(lambda r: r.name == k, guild.roles)
            if role:
                roles += [role]
        return roles

    @commands.slash_command(guild_only=True)
    async def namethrall(self, ctx: ApplicationContext) -> None:
        """Restore or relinquish your ability to change your nickname."""

        roles = self.get_roles(ctx.guild)
        if not roles or None in roles:
            text = ("Someone tried to use the name game feature, "
                    + "but the `namethrall` role(s) aren't set up.")
            await report_guild_error(self._bot, ctx, None, ctx.guild, text)
            return
        else:
            (namesub, namedom) = roles

        author = ctx.user
        log = "User requested via self-service command"
        if namedom in author.roles and namesub in author.roles:
            # {namedom && namesub} -> namesub
            await author.remove_roles(namedom, reason=log)
            reply = "your roles were messed up,, fixed it for you!! 🥰"
        elif namesub in author.roles:
            # namesub -> namedom
            await author.remove_roles(namesub, reason=log)
            await author.add_roles(namedom, reason=log)
            reply = "your rights have been restored... for now,, 😇"
        else:
            # {none | namedom} -> namesub
            await author.remove_roles(namedom, reason=log)
            await author.add_roles(namesub, reason=log)
            reply = "enjoy your subjugation~ 😈✨"

        await ctx.respond(reply, ephemeral=True)

    async def _change_nick(
        self,
        ctx: ApplicationContext,
        roles: list[Role],
        user: Union[User, Member],
        guild: Optional[Guild] = None,
        nickname: Optional[str] = None,
    ) -> None:
        member: Member = guild.get_member(user.id)
        (namesub, _) = roles
        if not (
            guild.get_member(ctx.author.id).guild_permissions.manage_nicknames
            or member.get_role(namesub.id)
        ):
            reply = (f"You don't have permission to change {user}'s nickname "
                     + f"and/or they don't have the {namesub.mention} role.")
            await ctx.respond(reply, ephemeral=True)
            return

        old_nick = member.nick or member.name
        if not nickname:
            nickname = member.name
        if old_nick == nickname:
            await ctx.respond("No", ephemeral=True)
            return

        reason = f'Namethrall re-assignment on behalf of {ctx.author} ("{old_nick}" -> "{nickname}")'
        try:
            await member.edit(nick=nickname, reason=reason)
        except Forbidden:
            reply = "Required permissions are missing, or target is higher in role hierarchy"
            await ctx.respond(reply, ephemeral=True)
        else:
            reply = f'Changed the nickname of `{user}` '
            if old_nick:
                reply += f'from "{old_nick}" '
            reply += f'to "{nickname}"'
            await ctx.respond(reply, ephemeral=True)

    @commands.slash_command()
    @option("user", Member, description="User reference; mention, nickname, username or ID")
    @option("nickname", str, description="New nickname to set (blank to reset)", default=None)
    @option("guild", Guild, description="Optional guild reference if issued in DMs", default=None)
    async def nick(
        self,
        ctx: ApplicationContext,
        user: Union[User, Member],
        nickname: Optional[str],
        guild: Optional[Guild],
    ) -> None:
        """"Changes the nickname of a namethrall."""

        if not guild and not ctx.guild:
            reply = f"You need to specify the guild for changing {user}'s nickname."
            await ctx.respond(reply)
            return
        elif not guild:
            guild = ctx.guild

        (namesub, namedom) = self.get_roles(guild)
        _configured = await self._is_guild_configured(
            ctx, namesub, namedom
        )
        if not _configured:
            return

        return await self._change_nick(
            ctx,
            (namesub, namedom),
            user,
            guild=guild,
            nickname=nickname,
        )


def setup(bot: Bot):
    bot.add_cog(NamethrallManager(bot))
