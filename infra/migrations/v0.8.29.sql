ALTER TABLE role_react_messages
    ADD COLUMN IF NOT EXISTS sweep bool NOT NULL DEFAULT 'false';