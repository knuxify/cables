#!/bin/bash

_ENV_FILE="$PWD/.env"
if [[ -f "$_ENV_FILE" && -r "$_ENV_FILE" ]]; then
  _log info 'Loading configuration from .env'
  set -a
  source "$_ENV_FILE"
  set +a
fi
