all_items = {};
changes = {};
lists = [];

function toggleSavebar() {
	if (Object.keys(changes).length > 0) {
		$(window).on('beforeunload', function() {
			return Object.keys(changes).length > 0;
		});
		$("#savebar").addClass('savebar-visible');
	} else {
		$(window).off('beforeunload');
		$("#savebar").removeClass('savebar-visible');
	}
}

function toggleSectionSensitivity(input) {
	if ($(input).prop('checked')) {
		$('#' + $(input).attr('section') + '-content').removeClass('disabled')
		$('#' + $(input).attr('section') + '-content :input:not(.remove-button)').each(function(){
			$(this).prop('disabled', false);
		});
	} else {
		$('#' + $(input).attr('section') + '-content').addClass('disabled')
		$('#' + $(input).attr('section') + '-content :input:not(.remove-button)').each(function(){
			$(this).prop('disabled', true);
		});
	}
}

function resetValues() {
	_changes = {};
	Object.assign(_changes, changes);
	Object.keys(changes).forEach(function(key){
		if (Array.isArray(changes[key])) {
			id = key.replace('.', '-');
			if (_changes[key].length != all_items[key].length) {
				if (_changes[key].length > all_items[key].length) {
					for (index = all_items[key].length; index < (_changes[key].length); index++) {
						removeListRow(id, index);
					};
				} else if (_changes[key].length < all_items[key].length) {
					for (index = _changes[key].length; index < (all_items[key].length); index++) {
						addListRow(id);
						$("#" + id + " .remove-button").eq(index).prop("disabled", false);
					};
				};
			};
			all_items[key].forEach(function(value, index, array) {
				$("#" + id + " .input-list-item").eq(index).find("select, input").each(function() {
					if (this.type == 'checkbox') {
						$(this).prop('checked', all_items[key][index]);
					} else {
						$(this).prop('value', all_items[key][index]);
					}
				});
			});
		} else {
			$("[name='" + key + "']").each(function(i) {
				if (this.type == 'checkbox') {
					$(this).prop('checked', all_items[key]);

					if ($(this).hasClass('section-toggle')) {
						toggleSectionSensitivity(input);
					}
				} else {
					$(this).prop('value', all_items[key]);
				}
			});
		}
	});
	changes = {};
	toggleSavebar();
}

function saveValues() {
	$.ajax({
	    url: window.location.href,
	    type: 'POST',
	    data: JSON.stringify(changes),
	    contentType: 'application/json; charset=utf-8',
	    dataType: 'json',
	    success: function(msg) {
			Object.keys(changes).forEach(function(key){
				all_items[key] = changes[key];
			});
	        changes = {};
			toggleSavebar();
	    }
	});
}

function onItemChange() {
	if (this.type == 'checkbox') {
		new_value = this.checked;
		if ($(this).hasClass('section-toggle')) {
			toggleSectionSensitivity(this);
		}
	} else {
		new_value = this.value;
	};

	if (changes.hasOwnProperty(this.name) && new_value == all_items[this.name]) {
		delete changes[this.name];
	} else if (new_value != all_items[this.name]) {
		changes[this.name] = new_value;
	}
	toggleSavebar();
}

function initInput(){
	if (this.name.endsWith(']')) {
		return;
	};

	if (this.type == 'checkbox') {
		all_items[this.name] = this.checked
		$( this ).change(onItemChange);
		if ($(this).hasClass('section-toggle')) {
			toggleSectionSensitivity(this);
		}

	} else if (this.type == 'text') {
		all_items[this.name] = this.value
		$( this ).keyup(onItemChange);

	} else {
		all_items[this.name] = this.value
		$( this ).change(onItemChange);
	};
}

/* Lists */

function onListItemChange() {
	key = this.name.split('[')[0];
	id = key.replace('.', '-');
	n = this.name.match(/\[(.*?)\]/)[1];

	// If the list is not yet in the changes dict, add it there
	// (the [... ] syntax copies the all_items dict, otherwise changes
	// get synced into all_items, which is... not ideal)
	if (!changes.hasOwnProperty(key)) {
		changes[key] = [...all_items[key]];
	}

	if (changes[key].includes(this.value) || (n != changes[key].length && !this.value)) {
		removeListRow(id, n);
	} else {
		changes[key][n] = this.value;
	}

	if (changes.hasOwnProperty(key)) {
		// If the changes list and all items list are the same,
		// delete the changes list
		if ((changes[key].length === all_items[key].length) &&
				changes[key].every(function (e, i) { return e === all_items[key][i]; })) {
			delete changes[key];
		}
	}

	if ($('#' + id + ' .input-list-item:last-child select').val()) {
		addListRow(id);
	};

	if (changes.hasOwnProperty(key))
		$("#" + id + " .input-list-item").eq(n).find(".remove-button").prop("disabled", !changes[key][n]);
	else if (n < all_items[key].length)
		$("#" + id + " .input-list-item").eq(n).find(".remove-button").prop("disabled", false);
	else
		$("#" + id + " .input-list-item").eq(n).find(".remove-button").prop("disabled", true);

	toggleSavebar();
}

function selfDestructButton() {
	key = this.name.split('[')[0];
	id = key.replace('.', '-');
	n = this.name.match(/\[(.*?)\]/)[1];
	removeListRow(id, n)
}

function initListInput() {
	if (this.value)
		all_items[key].push(this.value);
	if (this.type == 'text') {
		$( this ).off('keyup');
		$( this ).keyup(onListItemChange);
	} else {
		$( this ).off('change');
		$( this ).change(onListItemChange);
	};
}

function initListButton(i) {
	$(this).off("click");
	$(this).click(selfDestructButton);
};

function initList() {
	id = $(this).attr('id');
	key = $(this).attr('key');
	all_items[key] = [];
	$("#" + id + " select, #" + id + " input").each(initListInput);
	$("#" + id + " .remove-button").each(initListButton);
	if ($("#" + id + " select, #" + id + " input").last().val())
		addListRow(id);
	$("#" + id + " .remove-button").last().prop("disabled", true);
}

function getListControl(id, n) {
	item = $("#" + id + " .input-list-item").eq(n)
	find = item.find("select");
	if (find)
		return find.first();
	return item.find("input").first();
}

function addListRow(id) {
	list = $('#' + id);
	key = list.attr('key');
	i = list.children().length;

	// Clone the first row of the list. This will act as the template
	// for the new row.
	row = $('#' + id + ' .input-list-item:first-child').clone(true);
	list.append(row);

	// Set up the new row: set the name, clear the selection, make the
	// remove button sensitive

	control = getListControl(id, i);
	remove_button = row.children(".remove-button");

	$(control).prop("name", key + '[' + i + ']');
	$(control).val('');
	$(remove_button).prop("disabled", true);
	$(remove_button).prop("name", key + '[' + i + ']');
}

function removeListRow(id, n) {
	list = $('#' + id);
	row = $('#' + id + ' .input-list-item').eq(n);
	key = list.attr("key")
	control = getListControl(id, n);
	row.remove();

	update_key = function(i) {
		if ($(this).prop("name").includes(key))
			$(this).prop("name", key + '[' + i + ']');
	}

	$(list).find('select').each(update_key);
	$(list).find('input').each(update_key);
	$(list).find('.input-list-item > .remove-button').each(update_key);
	$(control).prop("name", key + '[' + n + ']');

	if (!changes.hasOwnProperty(key))
		changes[key] = [...all_items[key]];

	changes[key].splice(n, 1);
	// If the changes list and all items list are the same,
	// delete the changes list
	if ((changes[key].length === all_items[key].length) &&
			changes[key].every(function (e, i) { return e === all_items[key][i]; })) {
		delete changes[key];
	}

	toggleSavebar();
}

/* Sidebar */
function closeSidebar() {
	$("#content-sidebar").animate(
		{opacity: 0}, 200,
		function() { $("#content-sidebar").hide(); }
	);
}
function openSidebar() {
	$("#content-sidebar").show();
	$("#content-sidebar").animate(
		{opacity: 1}, 200, function() {}
	);
}

$(document).ready(function(){
	$("#config-form").trigger("reset");
	toggleSavebar();
	$("#reset-link").click(resetValues);
	$("#save-button").click(saveValues);
	$("#config-form :input:not(:parent.list-input)").each(initInput);
	$("#content-sidebar").hide();
	$("#content-sidebar .nav-shade").click(closeSidebar);
	$(".sidebar-open-button").click(openSidebar);
	$(".input-list").each(initList);

	//const choices = [];
	//$("select").each(function (i, obj) {
	//	choices.push(new Choices(obj));
	//});
});
