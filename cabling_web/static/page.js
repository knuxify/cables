/* Common JS for base page */

/* User menu */
function closeUserMenu() {
	$("#user-menu-popup").animate(
		{opacity: 0}, 75,
		function() { $("#user-menu-popup").hide(); }
	);
	$("#user-menu-button").animate(
		{borderBottomRightRadius: 6, borderBottomLeftRadius: 6}, 75, function() {}
	);
}
function openUserMenu() {
	$("#user-menu-popup").show();
	$("#user-menu-popup").animate(
		{opacity: 1}, 75, function() {}
	);
	$("#user-menu-button").animate(
		{borderBottomRightRadius: 0, borderBottomLeftRadius: 0}, 75, function() {}
	);
}
function toggleUserMenu() {
	if ($("#user-menu-popup").css("display") == "block") {
		closeUserMenu();
	} else {
		openUserMenu();
	};
}

$(document).ready(function(){
	$("#user-menu-popup").hide();
	$("#user-menu-button").click(toggleUserMenu);
});
