from discord.ext import ipc
from typing import Union, Optional
import json
from . import app


ipc_client = ipc.Client(secret_key=app.config_dict["web"]["secret"])


async def ipc_request(*args, **kwargs):
    """Wrapper for ipc_client.request that reconnects the client if needed."""
    global ipc_client
    try:
        return await ipc_client.request(*args, **kwargs)
    except (AttributeError, ConnectionResetError, RuntimeError):
        ipc_client = ipc.Client(secret_key=app.config_dict["web"]["secret"])
        return await ipc_client.request(*args, **kwargs)


async def get_bot_version() -> str:
    """Returns the bot's current version as a string."""
    return (await ipc_request("get_bot_version"))["version"]


async def get_joined_guild_ids() -> list[int]:
    """Returns a list of IDs of all guilds that the bot has joined."""
    joined_guilds = await ipc_request("get_joined_guild_ids")
    return joined_guilds["ids"]


async def get_config_schema() -> dict:
    """Gets the list of available config keys."""
    return await ipc_request("get_config_schema")


async def get_config_values(guild_id, keys) -> dict:
    """Gets the values of the provided keys."""
    return await ipc_request("get_config_values", guild_id=guild_id, keys=keys)


async def set_config_values(guild_id: str, data: str):
    """Sets the values of the provided keys."""
    return await ipc_request("set_config_values", guild_id=guild_id, data=data)


async def get_guild_data(guild_id: str) -> dict | bool:
    """Gets the data of the guild with the given ID."""
    result = await ipc_request("get_guild_data", guild_id=guild_id)
    if "error" not in result:
        return result
    return False


async def get_guild_stats(guild_id: str, days: Optional[int] = 7) -> dict | bool:
    result = await ipc_request("get_guild_stats", guild_id=guild_id, days=days)
    if "error" not in result:
        return result
    return False
