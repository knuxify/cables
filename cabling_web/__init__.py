import json
import os
import time

from quart import (
    Quart,
    redirect,
    url_for,
    request,
    render_template,
    send_from_directory,
)
from quart_discord import Unauthorized
from minify_html import minify
from typing import Optional

from cabling_bot.bot.config import get_with_env_override

app = None


class CablesWeb(Quart):
    version: str = None
    _last_version_check: int = 3600

    def __init__(self):
        super().__init__(__name__)

        # TODO: How does py-cord get the config_path variable?
        config_path = "configs"
        with open(f"{config_path}/bot.json", "r") as f:
            self.config_dict = json.load(f)

        if self.get_config("web.use_https"):
            protocol = "https"
        else:
            protocol = "http"
        callback_url = self.get_config("web.callback_url")
        port = self.get_config("web.port")
        self.url = f"{protocol}://{callback_url}:{port}"

        self.config["DISCORD_CLIENT_ID"] = self.get_config("discord.client_id")
        self.config["DISCORD_CLIENT_SECRET"] = self.get_config("discord.client_secret")
        self.config["DISCORD_REDIRECT_URI"] = self.url + "/auth/callback"

        self.secret_key = self.get_config("web.secret")
        if not self.get_config("web.use_https"):
            os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "true"

        @self.errorhandler(Unauthorized)
        async def redirect_unauthorized(e):
            return redirect(url_for("auth.login"))

        @self.context_processor
        async def set_version() -> dict:
            from .bot import get_bot_version

            if not self.version or time.time() - self._last_version_check >= 3600:
                self.version = await get_bot_version()
                self._last_version_check = time.time()
            return {"version": self.version}

        global app
        app = self

        self.after_request(self.minify_html)

        self.register_blueprints()

    def register_blueprints(self) -> None:
        from .auth import auth_bp

        self.register_blueprint(auth_bp)

        from .config import config_bp

        self.register_blueprint(config_bp)

        @self.route("/")
        async def index():
            return redirect(url_for("config.guilds_page"))

        @self.route("/invite")
        async def invite():
            from .auth import discord_session

            guild_id = request.args.get("guild_id", default=None, type=int)
            return await discord_session.create_session(
                scope=["bot"],
                guild_id=guild_id,
                disable_guild_select=bool(guild_id) or False,
            )

        @self.route("/robots.txt")
        async def robots():
            return await send_from_directory(self.static_folder, "robots.txt")

        @self.errorhandler(404)
        async def error_notfound(e):
            return await render_template("errors/404.html"), 404

        @self.errorhandler(403)
        async def error_notallowed(e):
            return await render_template("errors/403.html"), 403

        @self.errorhandler(500)
        async def error_servererror(e):
            return await render_template("errors/500.html", message=str(e)), 500

    def get_config(self, config_key, default=None) -> Optional[str]:
        maybe_value = get_with_env_override(config_key, self.config_dict)
        if maybe_value:
            self.config_dict[config_key] = maybe_value
        return maybe_value or default

    async def minify_html(self, response):
        if response.content_type != "text/html; charset=utf-8":
            return response

        try:
            html = await response.get_data(as_text=True)
        except TypeError:
            html = response.get_data(as_text=True)

        response.set_data(minify(html))
        return response


def create_app() -> CablesWeb:
    global app
    app = CablesWeb()
    return app
