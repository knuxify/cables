from quart import Blueprint, request, session, redirect, url_for, render_template
from quart_discord import requires_authorization
import quart_discord.exceptions
import jinja2.exceptions
from markupsafe import escape

from . import app
from .bot import (
    get_joined_guild_ids,
    get_config_schema,
    get_config_values,
    set_config_values,
    get_guild_data,
)
from .tools import update_user_safe, user_owns_guild, get_cached_guild_stats

config_bp = Blueprint("config", __name__, url_prefix="/guilds")

# Config page schema. This dictates what config keys are displayed on each page.
# Every page has to be added here. If the page doesn't have any keys, just add
# an empty dict.
#
# The schema format is as follows:
#  {
#    "page_name": {
#       "Section name (this will be displayed in the header)": [
#         "!section_toggle.key",
#         "other_key_to.display",
#       ]
#    }
#  }
#
# Keys are provided in a module.key format. page_name is the page URL; it's also
# used to make the name of the page (the code automatically makes it uppercase).
#
# Keys prefixed with ! are section toggles; these are displayed as a toggle next
# to the section header, and enable/disable an entire section. Use these in cases
# where the config options in a section don't have any effect when a feature is
# disabled. **Note that the section toggle key must be a boolean.**
#
# A single key can be used in multiple pages, or appear multiple times on one
# page. There is no warning for missing keys; if you'd like to check for them,
# you can use the following code snippet:
#
# ```
# schema_keys = []
# for page in config_page_schema.values():
# 	if page:
# 	  for keys in page.values():
# 		  for key in keys:
# 			  schema_keys.append(key.replace('!', ''))
# available_keys = await get_config_schema()
# print([k for k in available_keys.keys() if k not in schema_keys])
# ```

config_page_schema = {
    "overview": {},
    "guild": {
        "General": [
            "general.description",
            "moderation.guidelines_url",
            "moderation.acceptable_content_url",
            "general.service_announcements",
        ],
        "Channels": [
            "notifications.public_announce_channel",
            "moderation.report_channel",
            "privileged.mod_channel",
            "privileged.system_channel",
        ],
        "Roles": [
            "privileged.owner_proxy_role",
            "privileged.admin_role",
            "privileged.admin_proxy_role",
            "privileged.staff_role",
            "privileged.channel_moderator_role",
            "privileged.global_moderator_role",
        ],
        "Member invites": [
            "!feature_flags.invites",
            "invites.join_channel",
            "invites.user_limit",
        ],
        "Previews": ["!feature_flags.previews", "preview.channels"],
        "Statistics": [
            "!feature_flags.statistics",
            "statistics.summary_msg",
            "statistics.summary_channel",
            "statistics.summary_targets",
        ],
        "Miscelaneous": ["fun.greeting", "utility.persistent_threads"],
    },
    "moderation": {
        "Content scanning": [
            "!feature_flags.content_scanning",
            "content_scanning.automoderation",
            "content_scanning.channels",
            "content_scanning.staff_mentions",
        ],
        "DEFCON": [
            "!feature_flags.defcon",
            "defcon.bot_protection",
            "defcon.ownerless_safety",
            "defcon.webhook_id",
            "defcon.webhook_token",
        ],
        "Bans": ["moderation.ban_appeals", "moderation.bans_revoke_privs"],
        "User watch": [
            "!feature_flags.user_watch",
            "user_watch.interval",
            "user_watch.report_channel",
            "user_watch.staff_mentions",
        ],
        "Member verification": [
            "!feature_flags.member_verification",
            "member_verification.application_time_limit",
            "member_verification.purge_summaries",
            "member_verification.role",
        ],
        "Sanctions": [
            "!moderation.anonymous_sanctions",
            "moderation.dm_sanctionee",
            "moderation.require_sanction_reasons",
            "moderation.sanction_approval_quorum",
            "moderation.sanctions_mention_staff",
        ],
    },
    "notifications": {
        "Joins": [
            "notifications.account_age_threshold",
            "notifications.guild_joins",
            "notifications.guild_parts",
        ],
        "Invites": [
            "notifications.member_invites",
            "notifications.cables_invites",
            "notifications.other_bot_invites",
        ],
        "Kicks": [
            "notifications.staff_kicks",
            "notifications.cables_kicks",
            "notifications.other_bot_kicks",
        ],
        "Tombstones": [
            "!feature_flags.tombstones",
            "notifications.tombstone_exemptions",
        ],
        "Miscelaneous": [
            "notifications.profile_updates",
            "notifications.thread_watch_channels",
            "notifications.vc_activity",
        ],
    },
    "fun": {
        "Colors": [
            "colors.allowed_role",
            "colors.role_position",
            "stats.role_categories",
        ],
        "Rolls": [
            "!feature_flags.rolls",
            "rolls.dubs",
            "rolls.trips",
            "rolls.quads",
            "rolls.channels",
            "rolls.timestamp_matches",
        ],
        "Namethrall": ["namethrall.namedom_role", "namethrall.namesub_role"],
        "Miscelaneous": ["feature_flags.amulets"],
    },
}

section_descriptions = {
    "DEFCON": "Various protection features for your guild",
    "Member invites": "Allows members to create invites using the `invite create` command",
    "Tombstones": "Whether or not to send a channel notification when a message is deleted",
    "Rolls": "RNG-based message ID/timestamp rolls detection"
}

async def check_missing_elements():
    schema_keys = []
    for page in config_page_schema.values():
        if page:
          for keys in page.values():
              for key in keys:
                  schema_keys.append(key.replace('!', ''))
    available_keys = await get_config_schema()
    print('added', [k for k in available_keys.keys() if k not in schema_keys])
    print('removed', [k for k in schema_keys if k not in available_keys.keys()])

# Quick reference for key data (as extracted from the config schema):
#  - "id": internal ID of config function
#  - "module", "key": joined as "{module}.{key}", represent key name
#  - "type": type string (int, bool, str, list)
#  - "purpose": what the key is used for (generic, channel_id, role_id)
#  - "default": default value
#  - "title", "description": human readable title and description of config key
#  - "value": current key value


# Routes


@config_bp.route("/")
@requires_authorization
async def guilds_page():
    try:
        await update_user_safe()
    except quart_discord.exceptions.RateLimited:
        if "owned_guilds" not in session["user"] or not session["user"]["owned_guilds"]:
            return "Server is currently being ratelimited, try again later.", 500

    owned_guilds = session["user"]["owned_guilds"]

    # "managed_guilds" is for owned guilds where the bot is present,
    # "unmanaged_guilds" is for owned guilds where the bot hasn't been invited
    managed_guilds = []
    unmanaged_guilds = []

    bot_guilds = await get_joined_guild_ids()

    for id, guild in owned_guilds.items():
        if int(id) in bot_guilds:
            managed_guilds.append(guild)
        else:
            unmanaged_guilds.append(guild)

    return await render_template(
        "config/guilds.html",
        user=session["user"],
        managed_guilds=managed_guilds,
        unmanaged_guilds=unmanaged_guilds,
    )


@config_bp.route("/<int:guild_id>")
async def guild_redirect(guild_id: int):
    return redirect(url_for(".guild_settings", guild_id=guild_id, section="overview"))


@config_bp.route("/<int:guild_id>/<section>", methods=("GET", "POST"))
@requires_authorization
async def guild_settings(guild_id: int, section: str):
    await check_missing_elements()

    await update_user_safe()

    if not user_owns_guild(str(guild_id)):
        return await render_template("errors/403.html"), 403

    guild_data = await get_guild_data(guild_id)

    if request.method == "POST":
        data = (await request.data).decode("utf-8")

        await set_config_values(guild_id, data)

        return {"success": True}

    global config_page_schema

    if section not in config_page_schema.keys():
        return redirect(
            url_for(".guild_settings", guild_id=guild_id, section="overview")
        )

    config_data = {}
    _section_keys = []
    section_toggles = {}

    # Loop 1: fill in config data dict with config data skeleton
    config_schema = await get_config_schema()
    for subsection, keys in config_page_schema[section].items():
        config_data[subsection] = []
        for _key in keys:
            if _key.startswith("!"):
                # Keys starting with ! are interpreted as "section toggles";
                # see note next to config page schema at the top of the file.
                key = _key.replace("!", "")
                section_toggles[subsection] = config_schema[key]
            else:
                key = _key
                config_data[subsection].append(config_schema[key])
            _section_keys.append(key)

    # Loop 2: set config values in the config data
    values = await get_config_values(guild_id, _section_keys)
    for subsection, keys in config_page_schema[section].items():
        for config in config_data[subsection]:
            config["value"] = values[config["module"] + "." + config["key"]]
        if subsection in section_toggles.keys():
            section_toggles[subsection]["value"] = values[
                section_toggles[subsection]["module"]
                + "."
                + section_toggles[subsection]["key"]
            ]

    bot_guilds = await get_joined_guild_ids()
    if guild_id not in bot_guilds:
        return redirect(url_for("invite", guild_id=guild_id))

    guild_stats = {}
    if section == "overview":
        guild_stats = await get_cached_guild_stats(guild_id)

    global section_descriptions
    try:
        return await render_template(
            f"config/pages/{section}.html",
            user=session["user"],
            guild=guild_data,
            stats=guild_stats,
            section=section,
            config_data=config_data,
            section_toggles=section_toggles,
            section_descriptions=section_descriptions,
        )
    except jinja2.exceptions.TemplateNotFound:
        return await render_template(
            "config/config_base.html",
            user=session["user"],
            guild=guild_data,
            section=section,
            config_data=config_data,
            section_toggles=section_toggles,
            section_descriptions=section_descriptions,
        )
