# cables web UI

This is a work-in-progress web UI for cables. **It is not yet production-ready** - only enable it if you're 100% sure what you're doing!

See [SETUP.md](SETUP.md) for some basic information on how to set up the necessary config options to run it. You can run it for development using the `./run-web` script in the project's root directory.

## Adding a new settings page

- Add the page to the `config_page_schema` variable in `config.py`. See the comment there for an explaination of how to do this.
- Update the navbar in `templates/config/config_base.html` to add your page.

Optionally, you can add custom content before or after the config keys; to do this, add a template for your page in `templates/config/pages` and define the `beforekeys` or `afterkeys` blocks. See `templates/config/pages/overview.html` for an example of how you can do this.
