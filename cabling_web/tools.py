from quart import session, redirect, url_for
import quart_discord.models
import logging
import time

from .bot import get_guild_stats

logger = logging.getLogger(__name__)

# Both User and Guild have to_json() methods, but they don't return all of
# the information we need, so we use these two functions instead:


async def serialize_guild(guild: quart_discord.models.Guild) -> dict:
    """
    Takes a quart_discord.models.Guild object and serializes it into
    a Python dictionary.
    """
    _guild = {}

    _guild["id"] = guild.id
    _guild["name"] = guild.name
    _guild["icon_url"] = guild.icon_url
    _guild["is_owner"] = guild.is_owner

    return _guild


async def serialize_user(
    user: quart_discord.models.User, force_fetch: bool = False
) -> dict:
    """
    Takes a quart_discord.models.User object and serializes it into
    a Python dictionary.
    """
    _user = {}

    _user["id"] = user.id
    _user["username"] = user.username
    _user["discriminator"] = user.discriminator
    _user["avatar_url"] = user.avatar_url or user.default_avatar_url

    if force_fetch or not user.guilds:
        guilds = [await serialize_guild(guild) for guild in await user.fetch_guilds()]
    else:
        guilds = [await serialize_guild(guild) for guild in user.guilds]
    _user["guilds"] = guilds

    _user["owned_guilds"] = {}
    for guild in guilds:
        if guild["is_owner"]:
            _user["owned_guilds"][str(guild["id"])] = guild

    return _user


async def update_user() -> None:
    """
    Updates the user data stored in session data.

    Note that calling this method directly too many times may cause ratelimit
    issues; make sure to place this behind a sensible cooldown.
    """
    from .auth import discord_session

    user = await discord_session.fetch_user()
    session["user"] = await serialize_user(user, force_fetch=True)
    session["last_updated"] = time.time()


async def update_user_safe() -> None:
    """
    Updates the user data if the timeout has been reached.
    """
    if "last_updated" not in session:
        session["last_updated"] = 0
    if time.time() - session["last_updated"] >= 1800:
        await update_user()


def user_owns_guild(guild_id) -> bool:
    """
    Returns True if the user owns the guild, False otherwise.
    """
    if not session or "user" not in session or "owned_guilds" not in session["user"]:
        return False
    owned_guilds = session["user"]["owned_guilds"]
    return str(guild_id) in owned_guilds


_cached_guild_stats = {}


async def get_cached_guild_stats(guild_id: str) -> dict:
    """Gets the cached guild stats for the last 7 days."""
    if not user_owns_guild(guild_id):
        return False

    global _cached_guild_stats
    needs_update = False

    if guild_id in _cached_guild_stats:
        if time.time() - _cached_guild_stats[guild_id]["last_updated"] >= 1800:
            needs_update = True
    else:
        _cached_guild_stats[guild_id] = {}
        needs_update = True

    if needs_update:
        stats = await get_guild_stats(guild_id, days=7)
        _cached_guild_stats[guild_id]["stats"] = stats
        _cached_guild_stats[guild_id]["last_updated"] = time.time()
    else:
        stats = _cached_guild_stats[guild_id]["stats"]

    return stats
