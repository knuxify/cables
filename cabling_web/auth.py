from quart import Blueprint, request, session, redirect, url_for, render_template
from quart_discord import DiscordOAuth2Session, requires_authorization, Unauthorized
from . import app
from .tools import serialize_user

import oauthlib
import jwt.exceptions

discord_session = DiscordOAuth2Session(app)

auth_bp = Blueprint("auth", __name__, url_prefix="/auth")


def render_auth_error(message: str):
    return render_template("errors/auth.html", message=message)


@auth_bp.route("/login")
async def login():
    if "user" in session and session["user"]:
        return redirect(url_for("config.guilds_page"))
    return await discord_session.create_session()


@auth_bp.route("/logout")
async def logout():
    discord_session.revoke()
    session.clear()
    return redirect(url_for(".login"))


@auth_bp.route("/callback")
async def callback():
    try:
        await discord_session.callback()
        user = await discord_session.fetch_user()
        session["user"] = await serialize_user(user)
    except oauthlib.oauth2.rfc6749.errors.InvalidClientIdError:
        return render_auth_error("Authentication code has expired"), 500
    except jwt.exceptions.DecodeError:
        return (
            render_auth_error(
                "Callback URI does not match; did the maintainer forget to add the callback URI to the valid callback list?"
            ),
            500,
        )
    except oauthlib.oauth2.rfc6749.errors.MismatchingStateError:
        return (
            render_auth_error(
                "State does not match session. (This happens if you were logged out of your Discord account sometimes - try again.)"
            ),
            500,
        )
    return redirect(url_for("config.guilds_page"))
