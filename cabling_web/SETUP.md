# Web interface setup guide

(TODO: Move this to the wiki.)

## Setting up the OAuth2 callbacks

1. Navigate to [https://discord.com/developers/applications](https://discord.com/developers/applications).
2. Select the application you're using for the bot.
3. On the sidebar, select OAuth2 > General.

You will see the options for OAuth2 settings. Here's the information we'll need to input into the config:

- "Client ID" - `bot.client_id`
- "Client secret" - `bot.client_secret`. (Note that you need to click "Reset secret" to get the secret.)

Next, click the "Add redirect" button in the "Redirects" session, and add a redirect to `https://<YOURDOMAIN>:<PORT>/auth/callback`. (If your port is the standard port 80, omit the `:<PORT>` bit.)

In your config, set the `web.callback_url` value to **your domain only** (no port, no https://, no suffixes or trailing slashes).
