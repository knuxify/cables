from cabling_bot.util.statistics import get_targets
from discord import (HTTPException, Embed)

try:
    assert (
        getattr(self, "data")
        and self.data["DATE_HERE"]["candidates"]
    )
    msgs = self.data
except (AssertionError, AttributeError, KeyError):
    msgs = list()
    channels = get_targets(self._bot, ctx.guild)
    for c in channels:
        async for msg in c.history(limit=None, oldest_first=True):
            if (
                msg.type.name not in ("default", "reply")
                or msg.author.bot
                or msg.webhook_id
                or msg.flags.is_crossposted
            ):
                continue

        ## PUT YOUR CRITERIA HERE
        if (
            # msg.author.id == 0
            "search string" in msg.content.lower()
            or "another search string" in msg.content.lower()
        ):
            msgs.append(
                dict(c=msg.channel.id, m=msg.id)
            )

    self.data = {"DATE_HERE": {"candidates": msgs}}

try:
    if len(" ".join(msgs)) > 4000:
        embed = Embed(title="References", description=" ".join(msgs))
        await ctx.channel.send(embeds=[embed])
    else:
        await ctx.channel.send(" ".join(msgs))
except HTTPException:
    await ctx.channel.send(f"**{len(msgs)}** references found")
