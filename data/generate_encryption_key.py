import os
import pathlib
import stat
import sys
import nacl.utils
import nacl.secret
from git.repo import Repo

# key material location on disk
path = "./configs/encryption_key.bin"

# check execution path
cwd = os.getcwd()
if "cables" not in cwd:
    print("You must run this script from within the repository.")
    sys.exit(1)

# determine root path from anywhere within repo
cwd_parts = cwd.split("/")
while cwd_parts[-1] != "cables":
    cwd_parts.pop()
cwd = "/".join(cwd_parts)

# change directory to root, if necessary
repo_path = Repo(cwd).common_dir.removesuffix("/.git")
if cwd != repo_path:
    os.chdir(repo_path)

# try to create encryption key
with pathlib.Path(path) as f:
    try:
        assert f.resolve(strict=True)
    except FileNotFoundError:
        f.touch()
        f.write_bytes(nacl.utils.random(nacl.secret.SecretBox.KEY_SIZE))
        f.chmod(stat.S_IRUSR)  # 0400
    else:
        print(f"Object already exists at key path ({path}). Aborting!")
        sys.exit(1)
